# Flypedia

Flypedia is an open source online platform, designed to serve pilots with detailed information about airports and airfields worldwide.

The platform offers a Web interface to access data about airports and fields, including frequencies, runway details, diagrams and aerial photographs.

## Prerequisites

Before you begin, ensure you have the following installed on your system:
- Python 3.11 or higher

## Installation

Follow these steps to get your development environment set up:

1. Clone the repository:
```bash
git clone https://github.com/your-username/flypedia.git
cd flypedia
```

2. Install [Poetry](https://python-poetry.org/):
```bash
$ sudo apt install python3-poetry
```

or


```bash
curl -sSL https://install.python-poetry.org | python3 -
```

3. Install dependencies:
```bash
poetry install
```

4. To activate the virtual environment:
```bash
poetry shell
```

## Setting Up the `.env` File

Before running the project, you need to set up your environment variables. Copy the `.env.example` file to a new file named `.env` and fill in the values:

```bash
cp .env.example .env
```

Open the `.env` file in a text editor and update the variables with your specific settings.

## Preparing the data

Remember to run migrations and collect static files before starting the site in production:

```bash
poetry run python manage.py migrate
poetry run python manage.py collectstatic
```

## Running the Development Server

To start the development server and access the application via `0.0.0.0:8001`, run:

```bash
poetry run python manage.py runserver 0.0.0.0:8001
```

This will make the server accessible locally and on your network.

## Running with Production Configuration

For simulating a production environment, you can run the server with the production settings:

```bash
poetry run python manage.py runserver --settings=flypedia.settings.production
```

Make sure to configure the necessary environment variables and production settings before launching the server in this mode.

## Contributing

If you'd like to contribute to Flypedia, please fork the repository and use a feature branch. Pull requests are warmly welcome.

## License

Flypedia is released under the AGPLv3 License. See the bundled LICENSE file for details.

## Contact Information

If you have any questions or feedback, please write to xavier@antoviaque.org.
