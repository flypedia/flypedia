.PHONY: clean mentat

clean:
	find . -type f -name '*~' -delete
	find . -type f -name '*.pyc' -delete
	find . -type d -name '__pycache__' -exec rm -rf {} +

mentat:
	mentat `find -type f \( -name "*.py" -o -name "*.html" \) ! -path "*/migrations/*" ! -path "*/manage.py"`
