from django.test import TestCase
from django.urls import reverse
from wagtail.core.models import Page
from wagtail.search.models import Query

class SearchViewTest(TestCase):

    def setUp(self):
        # Set up non-modified objects used by all test methods
        self.root = Page.objects.get(id=1)
        self.searchable_page = self.root.add_child(instance=Page(title="Searchable", slug="searchable"))
        self.searchable_page.save_revision().publish()

    def test_search_view(self):
        # Test the search view with a query
        response = self.client.get(reverse('search') + '?query=Searchable')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Searchable")

    def test_search_no_results(self):
        # Test the search view with a query that has no results
        response = self.client.get(reverse('search') + '?query=NoResults')
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Searchable")

    def test_search_no_query(self):
        # Test the search view with no query
        response = self.client.get(reverse('search'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['search_results'], [])