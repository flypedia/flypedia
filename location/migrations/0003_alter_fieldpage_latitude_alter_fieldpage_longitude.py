# Generated by Django 4.2.7 on 2023-11-05 05:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0002_alter_fieldimage_caption_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fieldpage',
            name='latitude',
            field=models.DecimalField(blank=True, decimal_places=14, help_text="Latitude of the field (ex: '-16.722767')", max_digits=17, null=True),
        ),
        migrations.AlterField(
            model_name='fieldpage',
            name='longitude',
            field=models.DecimalField(blank=True, decimal_places=14, help_text="Longitude of the field (ex: '-151.466395')", max_digits=17, null=True),
        ),
    ]
