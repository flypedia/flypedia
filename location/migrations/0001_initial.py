# Generated by Django 4.2.7 on 2023-11-05 05:03

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields
import wagtail.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailcore', '0089_log_entry_data_json_null_to_object'),
        ('wagtailimages', '0025_alter_image_file_alter_rendition_file'),
    ]

    operations = [
        migrations.CreateModel(
            name='FieldPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Last updated')),
                ('elevation', models.IntegerField(blank=True, help_text='In feet (AMSL)', verbose_name='Field elevation')),
                ('pattern_altitude', models.IntegerField(blank=True, help_text='In feet (AMSL)')),
                ('overfly_altitude', models.IntegerField(blank=True, help_text='In feet (AMSL), if allowed', verbose_name='Overfly or approach altitude')),
                ('approach_instructions', wagtail.fields.RichTextField(blank=True, help_text='Instructions or restrictions to observe during approach, overfly or landing')),
                ('departure_instructions', wagtail.fields.RichTextField(blank=True, help_text='Instructions or restrictions to observe during takeoff or departure')),
                ('latitude', models.DecimalField(blank=True, decimal_places=6, help_text="Latitude of the field (ex: '-16.722767')", max_digits=9)),
                ('longitude', models.DecimalField(blank=True, decimal_places=6, help_text="Longitude of the field (ex: '-151.466395')", max_digits=9)),
                ('notes', wagtail.fields.RichTextField(blank=True, help_text="Additional notes which don't fit in the other fields. If another field                                      does exist for the information you're entering though, please enter it there!", verbose_name='Other notes')),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='Runway',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('number', models.IntegerField(help_text='Magnetic orientation of the runway', verbose_name='Runway number')),
                ('pattern', models.CharField(blank=True, help_text='Pattern direction: left, right, south...', max_length=50)),
                ('preferred_landing', models.BooleanField(default=False, help_text='By default, for example when wind is calm, is this runway preferred over other runways for landing?', verbose_name='Preferred runway for landing?')),
                ('preferred_takeoff', models.BooleanField(default=False, help_text='By default, for example when wind is calm, is this runway preferred over other runways for takeoff?', verbose_name='Preferred runway for takeoff?')),
                ('restrictions_landing', models.CharField(default=False, help_text='If landing on this specific runway is prohibited or must follow specific instructions, indicate them here', max_length=50, verbose_name='Restrictions for landing?')),
                ('restrictions_takeoff', models.CharField(default=False, help_text='If takeoff on this specific runway is prohibited or must follow specific instructions, indicate them here', max_length=50, verbose_name='Restrictions for takeoff?')),
                ('slope', models.DecimalField(blank=True, decimal_places=1, help_text='Inclination slope, in %', max_digits=4)),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='runways', to='location.fieldpage')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Frequency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('callsign', models.CharField(help_text="How the frequency is usually addressed on the radio (ex: 'Boston tower')", max_length=255)),
                ('frequency', models.CharField(help_text="The frequency in MHz (ex: '123.45')", max_length=7)),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='frequencies', to='location.fieldpage')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FieldImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('caption', models.CharField(blank=True, max_length=250)),
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailimages.image')),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='location.fieldpage')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
    ]
