from django.test import TestCase
from wagtail.models import Page
from location.models import FieldPage, Frequency, Runway, FieldImage
from wagtail.images.tests.utils import get_test_image_file

class FieldPageModelTest(TestCase):

    def setUp(self):
        # Set up non-modified objects used by all test methods
        self.root = Page.objects.get(id=1)
        self.field_page = FieldPage(title="Test Field", slug="test-field")
        self.root.add_child(instance=self.field_page)

        self.frequency = Frequency.objects.create(
            page=self.field_page,
            callsign="Test Callsign",
            frequency="123.45"
        )

        self.runway = Runway.objects.create(
            page=self.field_page,
            number=1,
            pattern="Left"
        )

        self.image = FieldImage.objects.create(
            page=self.field_page,
            image=get_test_image_file().image,
            caption="Test Caption"
        )

    def test_instance(self):
        # Test the FieldPage instance and its properties
        self.assertTrue(isinstance(self.field_page, FieldPage))
        self.assertEqual(self.field_page.title, "Test Field")

    def test_get_context(self):
        # Test the get_context method
        request = self.client.get(self.field_page.url)
        context = self.field_page.get_context(request)
        self.assertIn('page_tree', context)
        self.assertTrue(context['page_tree'].filter(id=self.field_page.id).exists())

    def test_field_page_fields(self):
        # Test all fields of FieldPage
        self.assertIsNone(self.field_page.elevation)
        self.assertIsNone(self.field_page.pattern_altitude)
        self.assertIsNone(self.field_page.overfly_altitude)
        self.assertEqual(self.field_page.approach_instructions, "")
        self.assertEqual(self.field_page.departure_instructions, "")
        self.assertIsNone(self.field_page.latitude)
        self.assertIsNone(self.field_page.longitude)
        self.assertEqual(self.field_page.notes, "")

    def test_frequency_model(self):
        # Test the Frequency model related to FieldPage
        self.assertEqual(self.frequency.callsign, "Test Callsign")
        self.assertEqual(self.frequency.frequency, "123.45")
        self.assertEqual(self.frequency.page, self.field_page)

    def test_runway_model(self):
        # Test the Runway model related to FieldPage
        self.assertEqual(self.runway.number, 1)
        self.assertEqual(self.runway.pattern, "Left")
        self.assertEqual(self.runway.page, self.field_page)

    def test_field_image_model(self):
        # Test the FieldImage model related to FieldPage
        self.assertTrue(self.image.image)
        self.assertEqual(self.image.caption, "Test Caption")
        self.assertEqual(self.image.page, self.field_page)
    # Add more tests for each field and method of FieldPage
