from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.admin.forms import WagtailAdminPageForm
from wagtail.admin.panels import FieldPanel, InlinePanel, FieldRowPanel
from wagtail.fields import RichTextField
from wagtail.models import Page, Orderable

class FieldPageForm(WagtailAdminPageForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Necessary to override the default title help_text, as this is a default wagtail form field
        title = self.fields['title']
        title.help_text="The name the field is most commonly referred to, for example on the radio"

class FieldPage(Page):
    updated = models.DateTimeField("Last updated", auto_now=True)
    elevation = models.IntegerField(blank=True, null=True, verbose_name="Field elevation", help_text="In feet (AMSL)")
    pattern_altitude = models.IntegerField(blank=True, null=True, help_text="In feet (AMSL)")
    overfly_altitude = models.IntegerField(blank=True, null=True, verbose_name="Overfly or approach altitude", help_text="In feet (AMSL), if allowed")
    approach_instructions = RichTextField(blank=True, null=True, help_text="Instructions or restrictions to observe during approach, overfly or landing")
    departure_instructions = RichTextField(blank=True, null=True, help_text="Instructions or restrictions to observe during takeoff or departure")
    latitude = models.DecimalField(blank=True, null=True, max_digits=17, decimal_places=14, help_text="Latitude of the field (ex: '-16.722767')")
    longitude = models.DecimalField(blank=True, null=True, max_digits=17, decimal_places=14, help_text="Longitude of the field (ex: '-151.466395')")
    notes = RichTextField(blank=True, null=True,
                          verbose_name="Other notes", 
                          help_text="Additional notes which don't fit in the other fields. If another field \
                                     does exist for the information you're entering though, please enter it there!")

    content_panels = Page.content_panels + [
        FieldRowPanel(
            [
                FieldPanel('elevation'),
                FieldPanel('pattern_altitude'),
                FieldPanel('overfly_altitude'),
            ],
            heading="Altitudes",
        ),
        FieldRowPanel(
            [
                FieldPanel('latitude'),
                FieldPanel('longitude'),
            ],
            heading="Coordinates",
        ),
        InlinePanel('frequencies',
                    heading="Radio frequencies",
                    label="Frequency",
                    help_text="Frequencies in use at the field (tower, ground, etc.) or around it (approach, departure, etc.)",
        ),
        InlinePanel('runways',
                    heading="Runways",
                    label="Runway",
                    help_text="Runways available on the field",
        ),
        FieldPanel('approach_instructions'),
        FieldPanel('departure_instructions'),
        InlinePanel('images',
                    heading="Diagrams & pictures",
                    label='Image',
                    help_text="Diagram or aerial pictures of the airport, showing elements like runways, taxiways, pattern, approach pattern, etc.",
        ),
        FieldPanel('notes'),
    ]

    base_form_class = FieldPageForm

    def get_context(self, request):
        context = super().get_context(request)
        root_page = Page.objects.get(id=1)
        context['page_tree'] = root_page.get_descendants().live().public()
        return context

class Frequency(Orderable):
    page = ParentalKey(FieldPage, on_delete=models.CASCADE, related_name='frequencies')
    callsign = models.CharField(max_length=255, help_text="How the frequency is usually addressed on the radio (ex: 'Boston tower')")
    frequency = models.CharField(max_length=7, help_text="The frequency in MHz (ex: '123.45')")

    panels = [
        FieldPanel('callsign'),
        FieldPanel('frequency'),
    ]

class Runway(Orderable):
    page = ParentalKey(FieldPage, on_delete=models.CASCADE, related_name='runways')
    number = models.IntegerField(verbose_name="Runway number", help_text="Magnetic orientation of the runway")
    length = models.IntegerField(blank=True, null=True, verbose_name="Runway length", help_text="Length of the runway, in feet")
    surface = models.CharField(blank=True, null=True, max_length=50, help_text="Surface type: grass, gravel, asphalt, concrete...")
    pattern = models.CharField(blank=True, null=True, max_length=50, help_text="Pattern direction: left, right, south...")
    preferred_landing = models.BooleanField(default=False, verbose_name="Preferred runway for landing?", help_text="By default, for example when wind is calm, is this runway preferred over other runways for landing?")
    preferred_takeoff = models.BooleanField(default=False, verbose_name="Preferred runway for takeoff?", help_text="By default, for example when wind is calm, is this runway preferred over other runways for takeoff?")
    restrictions_landing = models.CharField(blank=True, null=True, max_length=50, verbose_name="Restrictions for landing?", help_text="If landing on this specific runway is prohibited or must follow specific instructions, indicate them here")
    restrictions_takeoff = models.CharField(blank=True, null=True, max_length=50, verbose_name="Restrictions for takeoff?", help_text="If takeoff on this specific runway is prohibited or must follow specific instructions, indicate them here")
    slope = models.DecimalField(blank=True, null=True, max_digits=4, decimal_places=1, help_text="Inclination slope, in %")

    panels = [
        FieldPanel('number'),
        FieldPanel('length'),
        FieldPanel('surface'),
        FieldPanel('pattern'),
        FieldPanel('preferred_landing'),
        FieldPanel('preferred_takeoff'),
        FieldPanel('restrictions_landing'),
        FieldPanel('restrictions_takeoff'),
        FieldPanel('slope'),
    ]

class FieldImage(Orderable):
    page = ParentalKey(FieldPage, on_delete=models.CASCADE, related_name='images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, null=True, max_length=250)

    panels = [
        FieldPanel('image'),
        FieldPanel('caption'),
    ]
